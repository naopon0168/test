<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>login.jsp</title>
</head>
<body>

<h2>LoginJSP</h2>

<h2>「ログイン認証」画面</h2>
<hr>

<p>ようこそ「楽天市場」へ</p>

<p>ログインするには「会員ID」「パスワード」を入力し「ログイン」をクリックして下さい</p>

<form action="/webshop/LoginServlet" method="POST">

<table>

<tr>
<th>会員ID</th>
<td><input type="text" name="memberid"></td>
</tr>

<tr>
<th>パスワード</th>
<td><input type="password" name="password"></td>
</tr>

</table>

<input type="submit" name="command" value="ログイン">

</form>

<hr>

</body>
</html>