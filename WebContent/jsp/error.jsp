<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="bean.LoginCheck" %>
<!-- P198 リクエストスコープからインスタンスを取得 -->
<%
String message = (String) request.getAttribute("message");
%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>error.jsp</title>
</head>
<body>

	<h2>ErrorJSP</h2>

	<h2>「ログイン失敗」画面</h2>

<p><%= message %></p>
	<p>
		<a href="/webshop/jsp/login.jsp">「ログイン認証」へ戻る</a>
	</p>
</body>
</html>