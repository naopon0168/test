package bean;

public class LoginCheck {

	private final String MEMBER_ID = "12345678@aaaa";
	private final String PASS_WORD = "1234";

	private String message;

	public String checkLogin(String memberid, String password) {
		if(MEMBER_ID.contentEquals(memberid)&&PASS_WORD.contentEquals(password)) {
			message = "ログイン認証は完了しました♡";
		}
		else if(memberid.isEmpty() == true && password.isEmpty() == false) {
			message = "会員IDが未入力です";
		}
		else if(memberid.isEmpty() == false && password.isEmpty() == true) {
			message = "パスワードが未入力です";
		}
		else if(memberid.isEmpty() == true && password.isEmpty() == true) {
			message = "会員IDとパスワードが未入力です";
		}
		else {
			message = "入力情報が間違っています";
		}

		return message;
	}

}
