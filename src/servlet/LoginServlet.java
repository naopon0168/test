package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import bean.LoginCheck;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		//HttpSession session = request.getSession();

		LoginCheck loginCheck = new LoginCheck();

		//リクエストパラメータの取得
		//String url = null;
		String message = "";
		String memberid = request.getParameter("memberid");
		String password = request.getParameter("password");
		String command = request.getParameter("command");
		System.out.println("memberid：" + memberid + "\tpassword：" + password
				+ "\tcommand：" + command );

		message = loginCheck.checkLogin(memberid, password);
		//リクエストスコープに保存
		request.setAttribute("message", message);

		if(message.equals("ログイン認証は完了しました")) {
			//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/shopmain.jsp");
			dispatcher.forward(request, response);
		}
		else{
			//フォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/jsp/error.jsp");
			dispatcher.forward(request, response);
		}



		return;
	}

}
